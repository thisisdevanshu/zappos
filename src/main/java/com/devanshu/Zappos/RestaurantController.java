package com.devanshu.Zappos;

import com.devanshu.Zappos.DTOs.MenuItemDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;
import com.devanshu.Zappos.entity.Restaurant;
import com.devanshu.Zappos.services.MenuItemService;
import com.devanshu.Zappos.services.MenuService;
import com.devanshu.Zappos.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestaurantController {

    @Autowired
    RestaurantService restaurantService;

    @RequestMapping(value = "/restaurants" , method = RequestMethod.POST)
    public Long create(@RequestBody RestaurantDTO restaurantDTO) {
        try {
            return restaurantService.create(restaurantDTO);
        } catch (Exception e) {
            return 0L;
        }
    }
    @RequestMapping(value = "/restaurants/{id}" , method = RequestMethod.GET)
    public RestaurantDTO get(@PathVariable("id") Long id){
        try {
            RestaurantDTO restaurantDTO = restaurantService.get(id);
            return restaurantDTO;
        }catch(Exception e){
            return null;
        }
    }

    @RequestMapping(value = "/restaurants" , method = RequestMethod.GET)
    public List<RestaurantDTO> getAll(){

        try{
            return restaurantService.findAll();
        }catch(Exception e){
            return null;
        }
    }

    @RequestMapping(value = "/restaurants/{id}" , method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id){
        try {
            restaurantService.delete(id);
            return "deteleted restaurant "+id;
        }catch(Exception e){
            return "deletion failure";
        }
    }
}
