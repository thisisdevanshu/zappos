package com.devanshu.Zappos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZapposApplication {

	public static void main(String[] args) {

		SpringApplication.run(ZapposApplication.class, args);
	}
}
