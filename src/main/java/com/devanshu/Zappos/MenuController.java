package com.devanshu.Zappos;

import com.devanshu.Zappos.DTOs.MenuDTO;
import com.devanshu.Zappos.DTOs.MenuItemDTO;
import com.devanshu.Zappos.services.MenuItemService;
import com.devanshu.Zappos.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MenuController {

    @Autowired
    MenuService menuService;

    @RequestMapping(value = "/menu" , method = RequestMethod.POST)
    public Long create(@RequestBody MenuDTO menuDTO){
        try {
            return menuService.create(menuDTO);
        }catch(Exception e){
            return 0L;
        }
    }

    @RequestMapping(value = "/menu/{id}" , method = RequestMethod.GET)
    public MenuDTO get(@PathVariable("id") Long id){

        try {
            return menuService.get(id);
        }catch(Exception e){
            return  null;
        }
    }

    @RequestMapping(value = "/menu" , method = RequestMethod.GET)
    public List<MenuDTO> getAll(){

        try{
            return menuService.findAll();
        }catch(Exception e){
            return null;
        }
    }

    @RequestMapping(value = "/menu/{id}" , method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id){
        try {
            menuService.delete(id);
            return "deteleted restaurant "+id;
        }catch(Exception e){
            return "deletion failure";
        }
    }
}
