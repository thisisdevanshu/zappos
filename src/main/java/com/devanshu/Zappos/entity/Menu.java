package com.devanshu.Zappos.entity;

import javax.persistence.*;

@Entity
public class Menu {

    @Id
    @GeneratedValue
    Long id;
    String name;

    @ManyToOne(targetEntity = Restaurant.class)
    Restaurant restaurant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant rId) {
        this.restaurant = restaurant;
    }
}
