package com.devanshu.Zappos.entity;

import javax.persistence.*;

@Entity
public class MenuItem {

    @Id
    @GeneratedValue
    Long id;
    String name;

    @ManyToOne(targetEntity = Menu.class)
    Menu menu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu mId) {
        this.menu = menu;
    }
}
