package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.MenuDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;
import com.devanshu.Zappos.entity.Menu;
import com.devanshu.Zappos.entity.MenuRepository;
import com.devanshu.Zappos.entity.Restaurant;
import com.devanshu.Zappos.entity.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultMenuService implements MenuService {

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Override
    public Long create(MenuDTO menuDTO) throws  Exception{
        Menu menu = new Menu();
        menu.setName(menuDTO.getName());
        Restaurant restaurant = restaurantRepository.getOne(menuDTO.getRestaurant().getId());
        if(restaurant == null){
            throw new Exception("Invalid Restaurant");
        }
        menu.setRestaurant(restaurant);
        menuRepository.save(menu);
        return menu.getId();
    }
    @Override
    public MenuDTO get(Long id) throws  Exception{
        Menu menu = menuRepository.findOne(id);
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setId(menu.getId());
        menuDTO.setName(menu.getName());

        Restaurant restaurant = menu.getRestaurant();
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setId(restaurant.getId());
        restaurantDTO.setName(restaurant.getName());

        menuDTO.setRestaurant(restaurantDTO);
        return menuDTO;
    }
    @Override
    public List<MenuDTO> findAll() throws  Exception{
        List<Menu> menus = menuRepository.findAll();
        List<MenuDTO> menuDTOS = new ArrayList<>();
        for(Menu menu : menus){
            MenuDTO menuDTO = new MenuDTO();
            menuDTO.setId(menu.getId());
            menuDTO.setName(menu.getName());

            Restaurant restaurant = menu.getRestaurant();
            RestaurantDTO restaurantDTO = new RestaurantDTO();
            restaurantDTO.setId(restaurant.getId());
            restaurantDTO.setName(restaurant.getName());

            menuDTO.setRestaurant(restaurantDTO);
            menuDTOS.add(menuDTO);
        }
        return menuDTOS;
    }
    @Override
    public void delete(Long id) throws  Exception{
        menuRepository.delete(id);
    }
}
