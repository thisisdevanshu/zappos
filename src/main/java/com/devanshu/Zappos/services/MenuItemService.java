package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.MenuItemDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;

import java.util.List;

public interface MenuItemService {

    Long create(MenuItemDTO menuItem) throws  Exception;
    MenuItemDTO get(Long id) throws  Exception;
    List<MenuItemDTO> findAll() throws  Exception;
    void delete(Long id) throws  Exception;
}
