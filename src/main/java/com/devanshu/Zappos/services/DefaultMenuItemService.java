package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.MenuDTO;
import com.devanshu.Zappos.DTOs.MenuItemDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;
import com.devanshu.Zappos.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultMenuItemService implements MenuItemService {

    @Autowired
    MenuItemRepository menuItemRepository;

    @Autowired
    MenuRepository menuRepository;

    public Long create(MenuItemDTO menuItemDTO) throws  Exception{
        MenuItem menuItem = new MenuItem();
        menuItem.setName(menuItemDTO.getName());
        menuItem.setMenu(menuRepository.findOne(menuItemDTO.getMenu().getId()));
        menuItemRepository.save(menuItem);
        return  menuItem.getId();
    }
    public MenuItemDTO get(Long id) throws  Exception{
        MenuItem menuItem = menuItemRepository.findOne(id);
        MenuItemDTO menuItemDTO = new MenuItemDTO();
        menuItemDTO.setId(menuItem.getId());
        menuItemDTO.setName(menuItem.getName());

        Menu menu = menuItem.getMenu();
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setId(menu.getId());
        menuDTO.setName(menu.getName());

        Restaurant restaurant = menu.getRestaurant();
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setName(restaurant.getName());
        restaurantDTO.setId(restaurant.getId());

        menuDTO.setRestaurant(restaurantDTO);
        return menuItemDTO;
    }
    public List<MenuItemDTO> findAll() throws  Exception{
        List<MenuItem> menuItems = menuItemRepository.findAll();
        List<MenuItemDTO> menuItemDTOS = new ArrayList<>();
        for(MenuItem menuItem : menuItems){
            MenuItemDTO menuItemDTO = new MenuItemDTO();
            menuItemDTO.setId(menuItem.getId());
            menuItemDTO.setName(menuItem.getName());

            Menu menu = menuItem.getMenu();
            MenuDTO menuDTO = new MenuDTO();
            menuDTO.setId(menu.getId());
            menuDTO.setName(menu.getName());

            Restaurant restaurant = menu.getRestaurant();
            RestaurantDTO restaurantDTO = new RestaurantDTO();
            restaurantDTO.setName(restaurant.getName());
            restaurantDTO.setId(restaurant.getId());

            menuDTO.setRestaurant(restaurantDTO);
            menuItemDTOS.add(menuItemDTO);
        }
        return menuItemDTOS;
    }

    public void delete(Long id) throws  Exception{
        menuItemRepository.delete(id);
    }

}
