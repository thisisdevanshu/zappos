package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.RestaurantDTO;
import com.devanshu.Zappos.entity.Restaurant;
import com.devanshu.Zappos.entity.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultRestaurantService implements  RestaurantService {

    @Autowired
    RestaurantRepository restaurantRepository;

    @Override
    public Long create(RestaurantDTO restaurantDTO) throws  Exception{

        Restaurant restaurant = new Restaurant();
        restaurant.setName(restaurantDTO.getName());
        restaurantRepository.save(restaurant);
        return restaurant.getId();
    }

    @Override
    public RestaurantDTO get(Long id) throws  Exception{
        Restaurant restaurant = restaurantRepository.findOne(id);
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setName(restaurant.getName());
        restaurantDTO.setId(restaurant.getId());
        return restaurantDTO;
    }

    @Override
    public List<RestaurantDTO> findAll() throws  Exception{
        List<Restaurant> restaurants = restaurantRepository.findAll();
        List<RestaurantDTO> restaurantDTOS = new ArrayList<>();
        for(Restaurant restaurant : restaurants){
            RestaurantDTO restaurantDTO = new RestaurantDTO();
            restaurantDTO.setName(restaurant.getName());
            restaurantDTO.setId(restaurant.getId());
            restaurantDTOS.add(restaurantDTO);
        }

        return restaurantDTOS;
    }

    public void delete(Long id) throws  Exception{
        restaurantRepository.delete(id);
    }
}
