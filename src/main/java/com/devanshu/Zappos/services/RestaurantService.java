package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.RestaurantDTO;

import java.util.List;

public interface RestaurantService {

    Long create(RestaurantDTO restaurantDTO) throws  Exception;
    RestaurantDTO get(Long id) throws  Exception;
    List<RestaurantDTO> findAll() throws  Exception;
    void delete(Long id) throws  Exception;
}
