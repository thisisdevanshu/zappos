package com.devanshu.Zappos.services;

import com.devanshu.Zappos.DTOs.MenuDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;

import java.util.List;

public interface MenuService {

    Long create(MenuDTO menuDTO) throws  Exception;
    MenuDTO get(Long id) throws  Exception;
    List<MenuDTO> findAll() throws  Exception;
    void delete(Long id) throws  Exception;
}
