package com.devanshu.Zappos;

import com.devanshu.Zappos.DTOs.MenuItemDTO;
import com.devanshu.Zappos.DTOs.RestaurantDTO;
import com.devanshu.Zappos.services.MenuItemService;
import com.devanshu.Zappos.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MenuItemController {

    @Autowired
    MenuItemService menuItemService;

    @RequestMapping(value = "/menuItem" , method = RequestMethod.POST)
    public Long create(@RequestBody MenuItemDTO menuItemDTO){
        try {
            return menuItemService.create(menuItemDTO);
        }catch(Exception e){
            return 0L;
        }
    }

    @RequestMapping(value = "/menuItem/{id}" , method = RequestMethod.GET)
    public MenuItemDTO get(@PathVariable("id") Long id){
        try {
            return menuItemService.get(id);
        }catch(Exception e){
            return null;
        }
    }

    @RequestMapping(value = "/menuItem" , method = RequestMethod.GET)
    public List<MenuItemDTO> getAll(){
        try{
            return menuItemService.findAll();
        }catch(Exception e){
            return null;
        }
    }

    @RequestMapping(value = "/menuItem/{id}" , method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id){
        try {
            menuItemService.delete(id);
            return "deteleted restaurant "+id;
        }catch(Exception e){
            return "deletion failure";
        }
    }


}
